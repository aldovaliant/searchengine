/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package searchengine;

/**
 * @author michael n c 2016730009
 * @author aldo verrell valiant 2016730024
 * @author regen renaldo 2016730021
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

/**
 *
 * @author Dell
 */
public class SearchEngine {

    public static void main(String[] args) throws FileNotFoundException, IOException 
    {
        String path = "D:\\Kuliah\\PTKI\\DataSet";
        
        File[] files = findFilesInDirectory(path);
        Map<String, ArrayList> invertedIndex = new TreeMap<String, ArrayList> ();
        
        String[] stopWordArray = new String[] {"a", "about", "above", "above", "across", "after", "afterwards", "again", "against", "all", "almost", "alone", "along", "already", "also","although",
        "always","am","among", "amongst", "amoungst", "amount",  "an", "and", "another", "any","anyhow","anyone","anything","anyway", "anywhere", "are", "around", "as",  "at", "back","be",
        "became", "because","become","becomes", "becoming", "been", "before", "beforehand", "behind", "being", "below", "beside", "besides", "between", "beyond", "bill", "both", "bottom",
        "but", "by", "call", "can", "cannot", "cant", "co", "con", "could", "couldnt", "cry", "de", "describe", "detail", "do", "done", "down", "due", "during", "each", "eg", "eight", "either",
        "eleven","else", "elsewhere", "empty", "enough", "etc", "even", "ever", "every", "everyone", "everything", "everywhere", "except", "few", "fifteen", "fify", "fill", "find", "fire", "first",
        "five", "for", "former", "formerly", "forty", "found", "four", "from", "front", "full", "further", "get", "give", "go", "had", "has", "hasnt", "have", "he", "hence", "her", "here", "hereafter", 
        "hereby", "herein", "hereupon", "hers", "herself", "him", "himself", "his", "how", "however", "hundred", "ie", "if", "in", "inc", "indeed", "interest", "into", "is", "it", "its", "itself", "keep", 
        "last", "latter", "latterly", "least", "less", "ltd", "made", "many", "may", "me", "meanwhile", "might", "mill", "mine", "more", "moreover", "most", "mostly", "move", "much", "must", "my", "myself", 
        "name", "namely", "neither", "never", "nevertheless", "next", "nine", "no", "nobody", "none", "noone", "nor", "not", "nothing", "now", "nowhere", "of", "off", "often", "on", "once", "one", "only", 
        "onto", "or", "other", "others", "otherwise", "our", "ours", "ourselves", "out", "over", "own","part", "per", "perhaps", "please", "put", "rather", "re", "same", "see", "seem", "seemed", "seeming", 
        "seems", "serious", "several", "she", "should", "show", "side", "since", "sincere", "six", "sixty", "so", "some", "somehow", "someone", "something", "sometime", "sometimes", "somewhere", "still", 
        "such", "system", "take", "ten", "than", "that", "the", "their", "them", "themselves", "then", "thence", "there", "thereafter", "thereby", "therefore", "therein", "thereupon", "these", "they", 
        "thick", "thin", "third", "this", "those", "though", "three", "through", "throughout", "thru", "thus", "to", "together", "too", "top", "toward", "towards", "twelve", "twenty", "two", "un", "under",
        "until", "up", "upon", "us", "very", "via", "was", "we", "well", "were", "what", "whatever", "when", "whence", "whenever", "where", "whereafter", "whereas", "whereby", "wherein", "whereupon", 
        "wherever", "whether", "which", "while", "whither", "who", "whoever", "whole", "whom", "whose", "why", "will", "with", "within", "without", "would", "yet", "you", "your", "yours", "yourself", 
        "yourselves", "the"};
        
        ArrayList<String> stopWords = new ArrayList<String>(Arrays.asList(stopWordArray));
        
        for (int i = 0; i < files.length; i++) {
            
            System.out.println("---------------");
            System.out.println("nama file : " + files[i]);
            String namaDoc = files[i].getName();
            int titik = namaDoc.indexOf(".");
            namaDoc = namaDoc.substring(0, titik);
            System.out.println("nama doc : " + namaDoc);

            Scanner input = new Scanner(files[i]); 
 
            int count = 0;
            while (input.hasNext()) 
            {
                String word = input.next();
                word = word.toLowerCase();
                
                word = word.replaceAll("[\\p{Punct}&&[^-]]+", "");
                
                count = count + 1;

                if(!stopWords.contains(word)){
                    if (word.length() > 0)
                    {
                        ArrayList<String> valueWord = invertedIndex.get(word);
                        if (valueWord != null)
                        {
                            if (!valueWord.contains(namaDoc))
                            {
                                valueWord.add(namaDoc);
                                invertedIndex.put(word, valueWord);
                            }
                        }
                        else
                        {
                            ArrayList<String> postingList = new ArrayList<String>();
                            postingList.add(namaDoc);
                            invertedIndex.put(word, postingList);
                        }
                    }
                }
            }
            System.out.println("Word count: " + count);
        }
        
        invertedIndex.forEach((key, value) -> System.out.println(key + ":" + value));
    }
    
    public static File[] findFilesInDirectory(String directoryPath) {

        File folder = new File(directoryPath);
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                System.out.println("File " + listOfFiles[i].getName());
            } else if (listOfFiles[i].isDirectory()) {
                System.out.println("Directory " + listOfFiles[i].getName());
            }
        }

        System.out.println("banyak isi file : " + listOfFiles.length);
        return listOfFiles;
    }    
}
